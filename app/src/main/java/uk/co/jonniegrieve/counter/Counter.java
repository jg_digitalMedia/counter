package uk.co.jonniegrieve.counter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Counter extends AppCompatActivity {

    int ct = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);

    //Button button  find view by id method, cast to a button
    Button button = (Button) findViewById(R.id.counter_btn);
    final TextView textview = (TextView) findViewById(R.id.textView);

    button.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ct++;
                    textview.setText(ct + "");
            }

            }

            );
    }
}
